package io.certivity.backend;

import io.certivity.backend.page.PageContentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    @Order(1)
    CommandLineRunner commandLineRunner(PageContentRepository pageContentRepository) {
        return (String[] args) -> {
            var pages = pageContentRepository.findAll();
            System.out.printf("Application started with %s page contents %n", pages.size());
        };
    }
}
