package io.certivity.backend.scraper;

import org.springframework.boot.CommandLineRunner;

public interface Scraper extends CommandLineRunner { }
