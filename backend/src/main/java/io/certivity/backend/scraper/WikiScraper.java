package io.certivity.backend.scraper;

import io.certivity.backend.page.PageContentService;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WikiScraper implements Scraper {

    static String WIKI_PAGE = "https://en.wikipedia.org/wiki/A_Tale_of_Two_Cities";


    final PageContentService pageContentService;

    public WikiScraper(PageContentService pageContentService) {
        this.pageContentService = pageContentService;
    }

    /**
     * Resolves Task 1 and Task 2 and Task 3.
     */
    @Override
    @Order(0)
    public void run(String... args) throws Exception {
        if (pageContentService.getPageContent(WIKI_PAGE).isEmpty()) {
            WebDriver driver = new HtmlUnitDriver();
            try {
                driver.get(WIKI_PAGE);
                List<WebElement> searchResults = driver.findElements(By.cssSelector("p, h1, h2, h3, h4, h5, h6, ol, ul"));
                this.pageContentService.savePage(WIKI_PAGE, searchResults);
            } finally {
                driver.quit();
            }
        }
    }

}
