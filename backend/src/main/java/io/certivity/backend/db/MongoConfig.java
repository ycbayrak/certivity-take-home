package io.certivity.backend.db;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "io.certivity.backend.api")
public class MongoConfig extends AbstractMongoClientConfiguration {

    @Value("${MONGO_DB_HOST:localhost}")
    private String mongoHost;

    @Value("${MONGO_DB_PORT:27017}")
    private String mongoPort;

    @Override
    protected String getDatabaseName() {
        return "certivity-url-db";
    }

    @Override
    public MongoClient mongoClient() {
        return MongoClients.create(String.format("mongodb://%s:%s", mongoHost, mongoPort));
    }
}
