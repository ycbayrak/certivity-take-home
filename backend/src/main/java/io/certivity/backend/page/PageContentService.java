package io.certivity.backend.page;

import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.Instant;
import java.util.*;
import java.util.stream.IntStream;

@Service
public class PageContentService {

    final PageContentRepository pageContentRepository;

    public PageContentService(PageContentRepository pageContentRepository) {
        this.pageContentRepository = pageContentRepository;
    }

    public List<PageContentRetrieveDto> getPageContent(String pageUrl) {

        var content = this.pageContentRepository.findByUrlOrderBySortAsc(pageUrl);
        List<PageContentRetrieveDto> dtos = new ArrayList<>(Collections.emptyList());

        content.forEach((c) -> {
            dtos.add(PageContentRetrieveDto.from(c));
        });

        return dtos;
    }

    public void savePage(String url, List<WebElement> pageContentElements) {
        List<PageContent> pageContents = new ArrayList<>();

        IntStream.range(0, pageContentElements.size()).forEach(i -> {
            PageContent p = new PageContent();
            WebElement el = pageContentElements.get(i);
            p.setUrl(url);
            p.setHtml(el.getAttribute("innerHTML"));
            p.setText(el.getText());
            p.setLength(el.getText().length());
            p.setSort(i);
            p.setCreatedAt(Date.from(Instant.now()));
            p.setLastModified(Date.from(Instant.now()));
            pageContents.add(p);
        });

        this.pageContentRepository.saveAll(pageContents);
    }

    public List<PageContent> searchPagesByText(String searchTerm) {
        return this.pageContentRepository.findByTextContaining(searchTerm);
    }
}
