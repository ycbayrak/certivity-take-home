package io.certivity.backend.page;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class PageContentController {

    final PageContentService pageContentService;

    public PageContentController(PageContentService pageContentService) {
        this.pageContentService = pageContentService;
    }

    @GetMapping("/page-content")
    public ResponseEntity<List<PageContentRetrieveDto>> getPageContents() {
        var pageContent = pageContentService.getPageContent("https://en.wikipedia.org/wiki/A_Tale_of_Two_Cities");
        return ResponseEntity.ok().body(pageContent);
    }

    /**
     * Resolves 9.
     */
    @GetMapping("/search")
    public ResponseEntity<List<PageContentRetrieveDto>> searchPageContents(@RequestParam String q) {
        List<PageContent> searchResults = pageContentService.searchPagesByText(q);
        List<PageContentRetrieveDto> dtos = searchResults.stream()
                .map(PageContentRetrieveDto::from)
                .collect(Collectors.toList());

        return ResponseEntity.ok().body(dtos);
    }
}
