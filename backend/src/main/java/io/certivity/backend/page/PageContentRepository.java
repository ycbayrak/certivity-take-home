package io.certivity.backend.page;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface PageContentRepository extends MongoRepository<PageContent, String> {

    List<PageContent> findByUrl(String url);

    List<PageContent> findByUrlOrderBySortAsc(String url);

    @Query("{ 'text': { $regex: ?0, $options: 'i' } }")
    List<PageContent> findByTextContaining(String searchTerm);
}
