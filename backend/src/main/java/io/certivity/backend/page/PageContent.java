package io.certivity.backend.page;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Task 2
 */
@Document
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PageContent {

    private String url;
    private String text;
    private String html;
    private int length;
    private int sort;
    private Date createdAt;
    private Date lastModified;
}
