package io.certivity.backend.page;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class PageContentRetrieveDto {

    private String text;
    private String html;
    private int length;
    private int sort;
    private String url;
    private Date createdAt;
    private Date lastModified;

    public static PageContentRetrieveDto from(PageContent pageContent) {
        var dto = new PageContentRetrieveDto();
        dto.text = pageContent.getText();
        dto.html = pageContent.getHtml();
        dto.sort = pageContent.getSort();
        dto.url = pageContent.getUrl();
        dto.createdAt = pageContent.getCreatedAt();
        dto.lastModified = pageContent.getLastModified();

        return dto;
    }
}
