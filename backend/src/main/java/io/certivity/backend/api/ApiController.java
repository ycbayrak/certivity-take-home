package io.certivity.backend.api;

import io.certivity.backend.page.PageContentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {

    final PageContentService pageContentService;

    public record CustomResponse(String message) {
    }

    public ApiController(PageContentService pageContentService) {
        this.pageContentService = pageContentService;
    }

    @GetMapping("/")
    public ResponseEntity<CustomResponse> helloWorld() {
        return ResponseEntity.ok().body(new CustomResponse("Hello World!"));
    }

}

