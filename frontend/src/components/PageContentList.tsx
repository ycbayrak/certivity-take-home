import {FunctionComponent} from "react";

import {PageContentItem} from "./PageContentItem";
import {PageContent} from "../types/PageContent";

export interface PageContentListProps {
    content: PageContent[]
}

export const PageContentList: FunctionComponent<PageContentListProps> = (props) => {
    return (
        <div className="PageContentList">
            <span>{'https://en.wikipedia.org/wiki/A_Tale_of_Two_Cities'}</span>
            {props.content.map((page: PageContent, index: number) => (
                <PageContentItem item={page} key={index}></PageContentItem>
            ))}
        </div>
    );
}