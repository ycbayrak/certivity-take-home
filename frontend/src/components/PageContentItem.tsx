import {FunctionComponent} from "react";
import {PageContent} from "../types/PageContent.ts";

export interface PageContentItemProps {
    item: PageContent
}


export const PageContentItem: FunctionComponent<PageContentItemProps> = ({item}) => {
    return (<div className="PageContentItem pageContent border border-gray-300 rounded mb-4 p-2">
        <h2 className="font-bold text-lg mb-2">Index: {item.sort}</h2>
        <p className="mb-2">Text: {item.text}</p>
        <p className="mb-2">Length: {item.length}</p>
        <p className="mb-2">HTML: {item.html}</p>
    </div>)
}