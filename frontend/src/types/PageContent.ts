export type PageContent = {
    url: string;
    text: string;
    html: string;
    length: number;
    sort: number;
    createdAt: string;
    lastModified: string;
}
