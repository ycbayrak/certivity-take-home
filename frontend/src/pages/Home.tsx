import ApiService from '../api/ApiService'
import {PageContentList} from "../components/PageContentList";


function Home() {
    const {isFetching: fetchingPageContent, data: pageContent} = ApiService.useGetPageContent()

    return (
        <div className="h-screen">
            {fetchingPageContent && <div>Loading...</div>}
            {pageContent && (
                <PageContentList content={pageContent}/>
            )}
        </div>
    )
}

export default Home
